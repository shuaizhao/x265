# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/shuaizhao/x265/source/common/bitstream.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/bitstream.cpp.o"
  "/Users/shuaizhao/x265/source/common/common.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/common.cpp.o"
  "/Users/shuaizhao/x265/source/common/constants.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/constants.cpp.o"
  "/Users/shuaizhao/x265/source/common/cpu.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/cpu.cpp.o"
  "/Users/shuaizhao/x265/source/common/cudata.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/cudata.cpp.o"
  "/Users/shuaizhao/x265/source/common/dct.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/dct.cpp.o"
  "/Users/shuaizhao/x265/source/common/deblock.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/deblock.cpp.o"
  "/Users/shuaizhao/x265/source/common/frame.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/frame.cpp.o"
  "/Users/shuaizhao/x265/source/common/framedata.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/framedata.cpp.o"
  "/Users/shuaizhao/x265/source/common/intrapred.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/intrapred.cpp.o"
  "/Users/shuaizhao/x265/source/common/ipfilter.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/ipfilter.cpp.o"
  "/Users/shuaizhao/x265/source/common/loopfilter.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/loopfilter.cpp.o"
  "/Users/shuaizhao/x265/source/common/lowpassdct.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/lowpassdct.cpp.o"
  "/Users/shuaizhao/x265/source/common/lowres.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/lowres.cpp.o"
  "/Users/shuaizhao/x265/source/common/md5.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/md5.cpp.o"
  "/Users/shuaizhao/x265/source/common/param.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/param.cpp.o"
  "/Users/shuaizhao/x265/source/common/piclist.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/piclist.cpp.o"
  "/Users/shuaizhao/x265/source/common/picyuv.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/picyuv.cpp.o"
  "/Users/shuaizhao/x265/source/common/pixel.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/pixel.cpp.o"
  "/Users/shuaizhao/x265/source/common/predict.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/predict.cpp.o"
  "/Users/shuaizhao/x265/source/common/primitives.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/primitives.cpp.o"
  "/Users/shuaizhao/x265/source/common/quant.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/quant.cpp.o"
  "/Users/shuaizhao/x265/source/common/scalinglist.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/scalinglist.cpp.o"
  "/Users/shuaizhao/x265/source/common/shortyuv.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/shortyuv.cpp.o"
  "/Users/shuaizhao/x265/source/common/slice.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/slice.cpp.o"
  "/Users/shuaizhao/x265/source/common/threading.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/threading.cpp.o"
  "/Users/shuaizhao/x265/source/common/threadpool.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/threadpool.cpp.o"
  "/Users/shuaizhao/x265/source/common/version.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/version.cpp.o"
  "/Users/shuaizhao/x265/source/common/wavefront.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/wavefront.cpp.o"
  "/Users/shuaizhao/x265/source/common/yuv.cpp" "/Users/shuaizhao/x265/build/arm-linux/common/CMakeFiles/common.dir/yuv.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "EXPORT_C_API=1"
  "HAVE_INT_TYPES_H=1"
  "HIGH_BIT_DEPTH=0"
  "MACOS=1"
  "X265_ARCH_X86=1"
  "X265_DEPTH=8"
  "X265_NS=x265"
  "X86_64=1"
  "__STDC_LIMIT_MACROS=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/Users/shuaizhao/x265/source/."
  "/Users/shuaizhao/x265/source/common"
  "/Users/shuaizhao/x265/source/encoder"
  "."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
