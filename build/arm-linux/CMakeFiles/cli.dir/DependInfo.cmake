# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/shuaizhao/x265/source/input/input.cpp" "/Users/shuaizhao/x265/build/arm-linux/CMakeFiles/cli.dir/input/input.cpp.o"
  "/Users/shuaizhao/x265/source/input/y4m.cpp" "/Users/shuaizhao/x265/build/arm-linux/CMakeFiles/cli.dir/input/y4m.cpp.o"
  "/Users/shuaizhao/x265/source/input/yuv.cpp" "/Users/shuaizhao/x265/build/arm-linux/CMakeFiles/cli.dir/input/yuv.cpp.o"
  "/Users/shuaizhao/x265/source/output/output.cpp" "/Users/shuaizhao/x265/build/arm-linux/CMakeFiles/cli.dir/output/output.cpp.o"
  "/Users/shuaizhao/x265/source/output/raw.cpp" "/Users/shuaizhao/x265/build/arm-linux/CMakeFiles/cli.dir/output/raw.cpp.o"
  "/Users/shuaizhao/x265/source/output/reconplay.cpp" "/Users/shuaizhao/x265/build/arm-linux/CMakeFiles/cli.dir/output/reconplay.cpp.o"
  "/Users/shuaizhao/x265/source/output/y4m.cpp" "/Users/shuaizhao/x265/build/arm-linux/CMakeFiles/cli.dir/output/y4m.cpp.o"
  "/Users/shuaizhao/x265/source/output/yuv.cpp" "/Users/shuaizhao/x265/build/arm-linux/CMakeFiles/cli.dir/output/yuv.cpp.o"
  "/Users/shuaizhao/x265/source/x265.cpp" "/Users/shuaizhao/x265/build/arm-linux/CMakeFiles/cli.dir/x265.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "EXPORT_C_API=1"
  "HAVE_INT_TYPES_H=1"
  "HIGH_BIT_DEPTH=0"
  "MACOS=1"
  "X265_ARCH_X86=1"
  "X265_DEPTH=8"
  "X265_NS=x265"
  "X86_64=1"
  "__STDC_LIMIT_MACROS=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/Users/shuaizhao/x265/source/."
  "/Users/shuaizhao/x265/source/common"
  "/Users/shuaizhao/x265/source/encoder"
  "."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/shuaizhao/x265/build/arm-linux/CMakeFiles/x265-shared.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
